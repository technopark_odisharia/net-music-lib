package MusicLib::Model::Album;

use strict;
use warnings;
use utf8;
use feature ':5.10';

use Mouse;
use MusicLib::DB;

has id         => (is => 'ro', isa => 'Int', required => 1);
has username   => (is => 'ro', isa => 'Str', required => 1);
has title      => (is => 'ro', isa => 'Str', required => 1);
has band       => (is => 'ro', isa => 'Str', required => 1);
has year       => (is => 'ro', isa => 'Int', required => 1);

sub create {
    my ($pkg, %opts) = @_;

    my $database = MusicLib::DB->get();
    my $statement = $database->prepare('INSERT INTO albums (username, title, band, year) VALUES (?, ?, ?, ?)');
    $statement->execute($opts{user}, $opts{title}, $opts{band}, $opts{year});

    if (my $error = $statement->err) {
        $database->rollback;
        return $error;
    }
    else {
        $database->commit;
        return undef;
    }
}

sub all {
    my $pkg = shift;
    my $username = shift;

    my $database = MusicLib::DB->get();
    my $statement = $database->prepare('SELECT * FROM albums WHERE username = ?');
    $statement->execute($username);

    my $albums = $statement->fetchall_arrayref({});

    if ($statement->err) {
        return undef;
    }
    else {
        return $albums;
    }
}

sub read {
    my $pkg = shift;
    my $id = shift;

    my $database = MusicLib::DB->get();
    my $statement = $database->prepare('SELECT * FROM albums WHERE id = ?');
    $statement->execute($id);

    my $album = $statement->fetchrow_hashref();

    if (defined $album) {
        return MusicLib::Model::Album->new($album);
    }
    else {
        return undef;
    }
}

sub update {
    my ($pkg, %opts) = @_;

    my $database = MusicLib::DB->get();
    my $statement = $database->prepare('UPDATE albums SET title = ?, band = ?, year = ? WHERE id = ?');
    $statement->execute($opts{title}, $opts{band}, $opts{year}, $opts{id});

    if (my $error = $statement->err) {
        $database->rollback;
        return $error;
    }
    else {
        $database->commit;
        return undef;
    }
}

sub get_id {
    my $pkg = shift;
    my $title = shift;
    my $band = shift;

    my $database = MusicLib::DB->get();
    my $statement = $database->prepare('SELECT * FROM albums WHERE title = ? AND band = ?');
    $statement->execute($title, $band);

    my $album = $statement->fetchrow_hashref();

    if (defined $album) {
        return $album->{id};
    }
    else {
        return undef;
    }
}

sub delete {
    my $pkg = shift;
    my $id = shift;

    my $database = MusicLib::DB->get();
    my $statement = $database->prepare('DELETE FROM albums WHERE id = ?');
    $statement->execute($id);

    if (my $error = $statement->err) {
        $database->rollback;
        return $error;
    }
    else {
        $database->commit;
        return undef;
    }
}

1;
