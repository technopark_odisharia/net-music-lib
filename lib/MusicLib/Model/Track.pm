package MusicLib::Model::Track;

use strict;
use warnings;
use utf8;
use feature ':5.10';

use Mouse;
use MusicLib::DB;

use DDP;

has id           => (is => 'ro', isa => 'Int', required => 1);
has album_id     => (is => 'ro', isa => 'Int', required => 1);
has title        => (is => 'ro', isa => 'Str', required => 1);
has format       => (is => 'ro', isa => 'Str', required => 1);
has link         => (is => 'ro', isa => 'Str', required => 1);

sub create {
    my ($pkg, %opts) = @_;

    my $database = MusicLib::DB->get();
    my $statement = $database->prepare('INSERT INTO tracks (album_id, title, format, link) VALUES (?, ?, ?, ?)');
    $statement->execute($opts{album}, $opts{title}, $opts{format}, $opts{link});

    if (my $error = $statement->err) {
        $database->rollback;
        return $error;
    }
    else {
        $database->commit;
        return undef;
    }
}

sub all {
    my $pkg = shift;
    my $album_id = shift;

    my $database = MusicLib::DB->get();
    my $statement = $database->prepare('SELECT * FROM tracks WHERE album_id = ?');
    $statement->execute($album_id);

    my $tracks = $statement->fetchall_arrayref({});

    if ($statement->err) {
        return undef;
    }
    else {
        return $tracks;
    }
}

sub read {
    my $pkg = shift;
    my $id = shift;

    my $database = MusicLib::DB->get();
    my $statement = $database->prepare('SELECT * FROM tracks WHERE id = ?');
    $statement->execute($id);

    my $track = $statement->fetchrow_hashref();

    if (defined $track) {
        return MusicLib::Model::Track->new($track);
    }
    else {
        return undef;
    }
}

sub update {
    my ($pkg, %opts) = @_;

    my $database = MusicLib::DB->get();
    my $statement = $database->prepare('UPDATE tracks SET title = ?, format = ?, link = ? WHERE id = ?');
    $statement->execute($opts{title}, $opts{format}, $opts{link}, $opts{id});

    if (my $error = $statement->err) {
        $database->rollback;
        return $error;
    }
    else {
        $database->commit;
        return undef;
    }
}

sub delete {
    my $pkg = shift;
    my $id = shift;

    my $database = MusicLib::DB->get();
    my $statement = $database->prepare('DELETE FROM tracks WHERE id = ?');
    $statement->execute($id);

    if (my $error = $statement->err) {
        $database->rollback;
        return $error;
    }
    else {
        $database->commit;
        return undef;
    }
}

1;
