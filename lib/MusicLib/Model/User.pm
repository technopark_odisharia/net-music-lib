package MusicLib::Model::User;

use strict;
use warnings;
use utf8;
use feature ':5.10';

use Mouse;
use MusicLib::DB;

has name   => (is => 'ro', isa => 'Str', required => 1);
has hash   => (is => 'ro', isa => 'Str', required => 1);

sub create {
    my $pkg = shift;
    my $name = shift;
    my $hash = shift;

    my $database = MusicLib::DB->get();
    my $statement = $database->prepare('INSERT INTO users (name, hash) VALUES (?, ?)');
    $statement->execute($name, $hash);

    if (my $error = $statement->err) {
        $database->rollback;
        return $error;
    } else {
        $database->commit;
        return undef;
    }
}

sub read {
    my $pkg = shift;
    my $name = shift;

    my $database = MusicLib::DB->get();
    my $statement = $database->prepare('SELECT * FROM users WHERE name = ?');
    $statement->execute($name);

    my $user = $statement->fetchrow_hashref();

    if (defined $user) {
        return MusicLib::Model::User->new($user);
    } else {
        return undef;
    }
}

sub delete {
    my $pkg = shift;
    my $name = shift;

    my $database = MusicLib::DB->get();
    my $statement = $database->prepare('DELETE FROM users WHERE name = ?');
    $statement->execute($name);

    if (my $error = $statement->err) {
        $database->rollback;
        return $error;
    } else {
        $database->commit;
        return undef;
    }
}

sub all {
    my $pkg = shift;

    my $database = MusicLib::DB->get();
    my $statement = $database->prepare('SELECT * FROM users');
    $statement->execute();

    my $users = $statement->fetchall_arrayref({});

    if ($statement->err) {
        return undef;
    } else {
        return $users;
    }
}

1;
