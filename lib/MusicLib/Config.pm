package MusicLib::Config;

use strict;
use warnings;
use utf8;
use feature ':5.10';

use base 'Class::Singleton';
use FindBin;
use YAML::XS 'LoadFile';
use DBI;
use DDP;

sub _new_instance {
  my $pkg = shift;
  my $self  = bless {}, $pkg;
  my $filename_yaml = "$FindBin::Bin/../config/config.yaml";

  $self->{ config }  = LoadFile($filename_yaml);

  return $self;
}

sub get {
  $_[0]->instance()->{ config };
}

1;
