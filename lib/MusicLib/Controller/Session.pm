package MusicLib::Controller::Session;
use Mojo::Base 'Mojolicious::Controller';

use strict;
use warnings;
use utf8;
use feature ':5.10';

use Digest;
use Digest::MD5;
use Session::Token;

use MusicLib::Secret;
use MusicLib::Memcache;
use MusicLib::Model::User;
use MusicLib::Helper::UserHelper 'get_user';

sub new_session {
    my $self = shift;

    $self->render(logined => '');
}

sub create {
    my $self = shift;

    my $user = MusicLib::Model::User->read($self->param('username'));
    my $password = $self->param('password');

    my $bcrypt = Digest->new('Bcrypt');
    $bcrypt->cost(MusicLib::Secret->cost());
    $bcrypt->salt(MusicLib::Secret->salt());
    $bcrypt->add($password);
    my $digest = $bcrypt->b64digest;

    if (defined $user and $user->{hash} eq $digest) {
        my $token = Session::Token->new(length => 120)->get;

        MusicLib::Memcache->get()->set($token, $user->{name});

        $self->session(expiration => 3600 * 24 * 10);
        $self->session({token => $token});
        $self->redirect_to("/");
    }
    else {
        $self->flash({error => 'Wrong pass or name'});
        $self->redirect_to("/login", status => 400);
    }
}

sub destroy {
    my $self = shift;

    my $token = $self->session('token') || '';
    MusicLib::Memcache->get()->delete($token);

    $self->redirect_to("/login");
}

1;
