package MusicLib::Controller::User;
use Mojo::Base 'Mojolicious::Controller';

use strict;
use warnings;
use utf8;
use feature ':5.10';

use Digest;
use Digest::MD5;
use Session::Token;
use File::Path qw(make_path);

use MusicLib::Secret;
use MusicLib::Memcache;
use MusicLib::Model::User;
use MusicLib::Model::Album;
use MusicLib::Helper::UserHelper 'get_user';

sub new_user {
    my $self = shift;

    $self->render(logined => '');
}

sub create {
    my $self = shift;

    my $name = $self->param('name');
    my $password = $self->param('password');
    my $repeat = $self->param('repeat');

    if ($name !~ /^[a-z1-9_]+$/) {
        $self->flash({error => "Name should contain only lowercase latin characters, numberss and underscore and must be non-empty"});
        $self->redirect_to('/register', status => 400);
    }
    elsif (length $password < 6) {
        $self->flash({error => "Length of your password must be at least 6 characters"});
        $self->redirect_to('/register', status => 400)
    }
    elsif ($password ne $repeat) {
        $self->flash({error => "Passwords don't match!"});
        $self->redirect_to('/register', status => 400);
    }
    else {
        my $bcrypt = Digest->new('Bcrypt');
        $bcrypt->cost(MusicLib::Secret->cost());
        $bcrypt->salt(MusicLib::Secret->salt());
        $bcrypt->add($password);
        my $digest = $bcrypt->b64digest;

        my $result = MusicLib::Model::User->create($name, $digest);

        if (not defined $result) {
            `rm -rf ./public/$name`;
            make_path "public/$name";

            my $token = Session::Token->new(length => 120)->get;
            MusicLib::Memcache->get()->set($token, $name);
            $self->session(expiration => 3600 * 24 * 10);
            $self->session({token => $token});

            $self->redirect_to("/");
        }
        elsif ($result == 1062) {
            $self->flash({error => "User with such name already exists"});
            $self->redirect_to('/register', status => 400);
        }
        else {
            $self->flash({error => "Something went wrong... :()"});
            $self->redirect_to('/register', status => 500);
        }
    }
}

sub show {
    my $self = shift;

    my $name = $self->param('name');
    my $user = MusicLib::Model::User->read($name);

    if ( not defined $user ) {
        my $current_user = get_user($self);

        if ( $current_user eq $name ) {
            my $token = $self->session('token') || '';
            MusicLib::Memcache->get()->delete($token);
            $self->redirect_to( '/', status => 401 );
        }
        else {
            $self->redirect_to( '/', status => 404 );
        }
    }
    else {
        my $albums = MusicLib::Model::Album->all($name);
        $self->render( name => $user->{name}, logined => 1, albums => $albums );
    }
}

sub me {
    my $self = shift;

    my $user = get_user($self);

    if ( defined $user ) {
        $self->redirect_to("/user/$user");
    }
    else {
        $self->redirect_to( '/login', status => 401 );
    }
}

sub index {
    my $self  = shift;

    my $users = MusicLib::Model::User->all;

    if ( not defined $users ) {
        $self->redirect_to( '/', status => 500 );
    }
    else {
        my $names = [ map { $_->{name} } @$users ];
        $self->render( names => $names, logined => 1 );
    }
}

sub delete {
    my $self = shift;
    my $user = get_user($self);
    my $err  = MusicLib::Model::User->delete($user);
    if ($err) {
        $self->redirect_to("/");
    }
    else {
        # removing files
        `rm -rf ./public/$user`;
        $self->redirect_to("/login");
    }
}

1;
