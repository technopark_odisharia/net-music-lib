package MusicLib::Helper::UserHelper;

use strict;
use warnings;
use utf8;
use feature ':5.10';

use Exporter 'import';

our @EXPORT_OK = qw(get_user);

sub get_user {
  my $c = shift;

  my $token = $c->session('token') || '';
  return MusicLib::Memcache->get()->get($token);
}

1;
