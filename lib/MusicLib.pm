package MusicLib;
use Mojo::Base 'Mojolicious';


use MusicLib::Memcache;
use MusicLib::Helper::UserHelper 'get_user';

# This method will run once at server start
sub startup {
    my $self = shift;

    # Router
    my $r = $self->routes;

    # Authenticate based on name parameter
    my $auth = $r->under( sub {
        my $c = shift;
        my $name = get_user($c);
        $c->stash(logged => 1);

        # Authenticated
        return 1 if defined $name;

        # Not authenticated
        $c->flash({error => 'Not authenticated'});
        $c->redirect_to("/login", status => 401);
        return undef;
    });

    my $unlogged = $r->under( sub {
        my $c = shift;
        $c->stash(logged => '');
        return 1;
    });

    $unlogged->get('/register')->to('user#new_user');
    $unlogged->post('/user')->to('user#create');

    $unlogged->get('/login')->to('session#new_session');
    $unlogged->post('/login')->to('session#create');
    $unlogged->post('/logout')->to('session#destroy');

    $auth->get('/user/:name')->to('user#show');
    $auth->get('/')->to('user#me');
    $auth->get('/users')->to('user#index');
    $auth->post('/user/delete')->to('user#delete');

    $auth->get('/album/new')->to('album#new_album');
    $auth->post('/album')->to('album#create');
    $auth->get('/album/id:id')->to('album#show');
    $auth->get('/album/id:id/edit')->to('album#edit');
    $auth->post('/album/id:id/edit')->to('album#update');
    $auth->get('/album/id:id/delete')->to('album#delete');

    $auth->get('/album/id:id/track/new')->to('track#new_track');
    $auth->post('/album/id:id/track')->to('track#create');
    $auth->get('/track/id:id/edit')->to('track#edit');
    $auth->post('/track/id:id/edit')->to('track#update');
    $auth->get('/track/id:id/delete')->to('track#destroy');
    $auth->get('/track/id:id/image')->to('track#add_image');
    $auth->post('/track/id:id/image')->to('track#post_image');
    $auth->get('/track/id:id/image/delete')->to('track#delete_image');

    $auth->get('/table')->to('table#new_table');
    $auth->post('/table')->to('table#create');
}

1;
